var _board = new Array(3);
var _turn;

var _Cross_selected_x;
var _Cross_selected_y;

var _Ball_selected_x;
var _Ball_selected_y;	

function clearCell(x,y){
	var cell;
	_board[x][y] = 0;
	cell = document.getElementById('c'+x+y);
	cell.innerHTML = "";
}

function ClearBoard(){
	for (var i = 0; i < _board.length; i++) {
		for (var ii = 0; ii < _board.length; ii++) {
			clearCell(i,ii);
		}
	}
}

function CheckCell(x,y){
	crosses = CheckTurn_Count(2);
	if (crosses==3) {
		if (_board[x][y]==2) {
			_Cross_selected_x = x;	
			_Cross_selected_y = y;
			clearCell(x,y);
		}
	}else{
		if(_board[x][y]==0 && DifMov(x,y)) selectCell(x,y);
	}
}

function selectCell(x,y){
	PaintCell(x,y);
	SearchMove();
}

function autoplay(){
	for (var i = 0; i < _board.length; i++) {
		_board[i] = new Array(3);
	}

	_turn = 'circulo';

	_Cross_selected_x=4;
	_Cross_selected_y=4;

	_Ball_selected_x=4;
	_Ball_selected_y=4;	

	ClearBoard();
	SearchMove();
}

function PaintCell(x,y){
	var cell;
	cell = document.getElementById('c'+x+y);
	cell.innerHTML = "<img src='"+_turn+".jpg'>";

	if(_turn == "circulo"){
		_board[x][y] = 1;
		_Ball_selected_x=x;
		_Ball_selected_y=y;	
	}else{
		_Cross_selected_x=x;
		_Cross_selected_y=y;
		_board[x][y] = 2;
	}

	CheckLine();

	if(_turn == "circulo" ){
		_turn = "equis";
	}else{
		_turn = "circulo";
	}
}

autoplay();