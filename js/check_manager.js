function CheckRoW(x ,value){
	c_value = 0;
	for (var i = 0; i < 3; i++) {
		if (_board[i][x]==value) {c_value++;}
	}
	return c_value;
}

function CheckDiag(d ,value){
	c_value = 0;

	if(_board[(1+d)][0] == value) c_value++;
	if(_board[1][1] == value) c_value++;
	if(_board[(1-d)][2] == value) c_value++;

	return c_value;
}

function CheckColumn(y ,value){
	c_value = 0;
	for (var i = 0; i < 3; i++) {
		if (_board[y][i]==value) {c_value++;}
	}
	return c_value;
}

function CheckLine(){
	if(_turn=="circulo") value =1;
	else value = 2;

	//gana fila
	if(CheckRoW(0 ,value)==3) {alert(_turn+ "Ha Ganado");}
	if(CheckRoW(1 ,value)==3) {alert(_turn+ "Ha Ganado");}
	if(CheckRoW(2 ,value)==3) {alert(_turn+ "Ha Ganado");}
	//gana columna
	if(CheckColumn(0 ,value)==3) {alert(_turn+ "Ha Ganado");}
	if(CheckColumn(1 ,value)==3) {alert(_turn+ "Ha Ganado");}
	if(CheckColumn(2 ,value)==3) {alert(_turn+ "Ha Ganado");}
	//gana diag
	if(CheckDiag(1 ,value)==3) {alert(_turn+ "Ha Ganado");}
	if(CheckDiag(-1 ,value)==3) {alert(_turn+ "Ha Ganado");}


}

function CheckTurn_Count(t_value){
	var Turn_count = 0;
	for (var i = 0; i < _board.length; i++) {
		for (var ii = 0; ii < _board.length; ii++) {
			if (_board[i][ii]==t_value) Turn_count ++;
		}
	}

	return Turn_count;
}

function DifMov(x, y){
	dif = false;

	if (_turn == 'equis') {
		if(x != _Cross_selected_x) dif= true;
		if(y != _Cross_selected_y) dif= true;
	}else{
		if(x != _Ball_selected_x) dif= true;
		if(y != _Ball_selected_y) dif= true;
	}

	return dif;
}