function SearchMove(){
		
	circulos = CheckTurn_Count(1);	
	if(circulos==3){
		var CellAv = false;

		while(!CellAv){
			x = Math.round(Math.random() * 2);
			y = Math.round(Math.random() * 2);
			CellAv = (_board[x][y]==1) ? true : false;
		}
		_Ball_selected_x =x;
		_Ball_selected_y =y;
		clearCell(x,y);
	}
	RandomMove();
}

function RandomMove(){
	var CellAvilable = false;

	while(!CellAvilable){
		x = Math.round(Math.random() * 2);
		y = Math.round(Math.random() * 2);
		CellAvilable = (_board[x][y]==0 && DifMov(x, y)) ? true : false;
	}
	PaintCell(x,y);	
}